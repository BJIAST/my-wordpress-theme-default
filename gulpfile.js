const gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS = require('gulp-clean-css'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    terser = require('gulp-terser'),
    browserSync = require('browser-sync').create();

const themename = 'ave';

function stylesDev() {
    return gulp.src('./src/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('styles-assembly.css'))
        .pipe(gulp.dest('./wp-content/themes/' + themename + '/css'))
        .pipe(browserSync.stream());
}

function scripts() {
    return gulp.src('./src/js/**/*.js')
        .pipe(terser())
        .pipe(concat('js-assembly.js'))
        .pipe(uglify({
            toplevel: true
        }))
        .pipe(gulp.dest('./wp-content/themes/' + themename + '/js'))
        .pipe(browserSync.stream());
}

function watch() {
    browserSync.init({
        proxy: 'ave.os',
        tunnel: false
    });

    gulp.watch('./src/scss/**/*.scss', stylesDev);
    gulp.watch('./src/js/**/*.js', scripts);
    gulp.watch('./**/*.php', browserSync.reload);
}

gulp.task('watch', watch);
